## Création d'une nouvelle instance

Suivre la procédure ci-dessous pour créer une nouvelle instance multilingue FR / EN dans la ferme Drupal i3S sur « webcms » :

### *Préliminaires :*

Si « xxx » est le nom de l'instance :

* créer une base de données « dm_xxx » (`utf8mb4` / `utf8mb4_unicode_ci`),
* créer le dossier « ./web/sites/xxx »" avec son contenu de base correctement paramétré,
* créer les dossiers « ./config/xxx » et « ./private/xxx » avec leur contenu de base,
* positionner les permissions de ces nouveaux dossiers,
* mettre en place l'alias sur le serveur web.

### *Création de l'instance :*

* Ouvrir la page de l'url correspondante avec un navigateur.
* choisir la langue française et lancer l'installation,
* suivre la procédure jusqu'à la fin.

### *Paramétrage :*

Une fois dans l'interface d'administration :

* installer la langue anglaise (/admin/config/regional/language) ;
* placer le sélecteur de langue (/admin/structure/block) ;
* paramétrer la détection / sélection choisie (/admin/config/regional/language/detection) ;
* activer la sélection de langue pour *contenu* / *bloc de contenu* / *lien de menu* / *lien de raccourci*, laisser *alias d'URL* tel quel (/admin/config/regional/content-language) ;
* traduire les options de menu principal (/admin/structure/menu/manage/main) ;
* traduire les contenus (actions -> traduire) (/admin/content).