## Description

This is a Drupal 10 installation profile tailored to i3S requirements, enabling on the fly site creation for immediate use.

### *Requirements:*

* [Language Switcher Dropdown](https://www.drupal.org/project/lang_dropdown),
* [Simple Menu Permissions](https://www.drupal.org/project/simple_menu_permissions);

plus [all i3S modules and the related sub-theme](https://gitlab.com/i3s/sg/drupal).


### *Features:*

* installs all required modules,
* sets “[i3s Olivero](https://gitlab.com/i3s/sg/drupal/i3s_olivero)” (see above) as default theme,
* sets specific configuration items,
* creates administration shortcuts,
* etc.

![Configuration form](config.png)

Maintainer: [ppomedio](https://gitlab.com/ppomedio)
