<?php

/**
 * @file
 * Enables modules and site configuration for a I3S site installation.
 */

#use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function i3s_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Pre-populate some of the configuration options.
  $form['site_information']['site_name']['#default_value'] = "i3S base";
  $form['site_information']['site_mail']['#default_value'] = "do_not_reply@webcms.i3s.unice.fr";
  #$form['admin_account']['account']['name']['#default_value'] = "";
  #$form['admin_account']['account']['mail']['#default_value'] = "";
  $form['regional_settings']['site_default_country']['#default_value'] = "FR";
  $form['regional_settings']['date_default_timezone']['#default_value'] = "Europe/Paris";
  $form['update_notifications']['enable_update_status_module']['#default_value'] = 0;
  $form['update_notifications']['enable_update_status_emails']['#default_value'] = 0;
}

